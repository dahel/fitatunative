import React, { Component } from 'react'
import { Provider } from 'react-redux';

import {
	AppRegistry,
	StyleSheet,
	Text,
	View,
	Navigator
} from 'react-native';

import moment from 'moment';
import 'moment/locale/pl';

moment.locale('pl', {
	calendar: {
		lastDay: '[Wczoraj]',
		sameDay: '[Dzisiaj]',
		nextDay: '[Jutro]',
		sameElse:  'DD MMM'
	}
});

import App from './components/App'
import createStore from './store/development';

const store = createStore();

export default class Main extends Component {
	render () {
		return (
			<Provider store={store}>
				<App />
			</Provider>
		)
	}
};
