import { Map } from 'immutable';

import apiClient from '../../modules/apiClient';

const init = Map({
	loading: false,
	errorResponse: null,
	token: '',
	username: '',
	password: ''
});


export default function authorization(store=init, action) {
	switch (action.type) {

		case 'LOGIN_REQUEST':
		case 'REGISTER_REQUEST':
			return store.set('loading', true);

		case 'LOGIN_SUCCESS':
		case 'REGISTER_SUCCESS':
			//console.log('################################################### LOGIN_SUCCESS',action.payload);
			apiClient.defaults.headers.Authorization = `Bearer ${action.payload}`;

			return store.merge({loading: false, token: action.payload});

		case 'REGISTER_FAILURE':
		case 'LOGIN_FAILURE':
			return store.set('loading', false);

		case 'LOGOUT':
			return store.merge({token: null});

		case 'INITIALIZATION_COMPLETE':
			if (action.payload) {
				apiClient.defaults.headers.Authorization = `Bearer ${action.payload}`;

				return store.merge({token: action.payload});
			}

			return store;

		default:
			return store;
	}
}