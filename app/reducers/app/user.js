import { Map } from 'immutable';
import jwtDecode from 'jwt-decode';

const init = Map({

});


export default function authorization(store=init, action) {
	switch (action.type) {
		case 'INITIALIZATION_COMPLETE':
		case 'LOGIN_SUCCESS':
			return store.merge(jwtDecode(action.payload));

		default:
			return store;
	}
}