import { Map, List } from 'immutable';
import moment from 'moment';

export function generateDates(selectedDate) {
    let dates = [];
    let startFrom = moment(selectedDate).subtract('days', 25).format('YYYY-MM-DD');
    let numberOfDates = 50;
    let i = 0;

    while (i < numberOfDates) {
        dates.push(moment(startFrom).add('days', i).format('YYYY-MM-DD'));

        i++;
    }

    return dates;
}