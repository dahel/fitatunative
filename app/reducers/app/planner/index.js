import { Map, List } from 'immutable';
import moment from 'moment';

import logger from '../../../modules/logger';
import {generateDates} from './dateCarousel';

const log = logger.get('planner reducer');

const init = Map({
	loading: true,
	selectedDate: moment().format('YYYY-MM-DD'),
	data: Map(),
	dates: generateDates(moment().format('YYYY-MM-DD'))
});



//function sumCalories(arr) {
//	debugger;
//}
//

function sumValue(valueKey, items) {
	return items.reduce(function (previousValue, currentValue) {
		return previousValue + currentValue[valueKey];
	}, 0)
}

function parsePlannerItem(itemData) {
	return {
		summary: {
			energy: sumValue('energy', itemData),
			carbohydrate: sumValue('carbohydrate', itemData),
			protein: sumValue('protein', itemData),
			fat: sumValue('fat', itemData)
		},
		items: itemData
	};
}

function parseDietPlan(data) {
	let keys = Object.keys(data);
	let parsed = {};

	keys.forEach(function (key) {
		parsed[key] = parsePlannerItem(data[key])
	});

	return parsed;
}

function parseActivityPlan(data) {
	return {
		summary: {
			energy: sumValue('energy', data)
		},
		items: data
	}
}

function updateStore(store, data) {
	let updatedStore = {};

	updatedStore.dietPlan = parseDietPlan(data.dietPlan);
	updatedStore.activityPlan = parseActivityPlan(data.activityPlan);

	return store.merge({loading: false, dietPlan: updatedStore.dietPlan, activityPlan: updatedStore.activityPlan});
}

function parseDayData(dayData) {
	return {
		dietPlan: parseDietPlan(dayData.dietPlan),
		activityPlan: parseActivityPlan(dayData.activityPlan),
		loading: false
	};
}

function setInitialData(store, data) {
	log.log('setInitialData()', data);

	let parsedData = {};
	let keys = Object.keys(data);

	keys.forEach((key) => {
		parsedData[key] = parseDayData(data[key])
	});

	return store.merge({loading: false, data: parsedData});
}

export default function planner(store=init, action) {
	//log.log(action.type, action);

	switch (action.type) {

		case 'FETCH_PLANNER_DATA_REQUEST':
			return store.set('loading', true);

		case 'FETCH_PLANNER_DATA_SUCCESS':
			console.log('todo');
			debugger;
			return store;

		case 'PLANNER_CHANGE_DATE':
			return store.set('selectedDate', action.date);

		case 'LOAD_INITIAL_PLANNER_DATA_SUCCESS':
			return setInitialData(store, action.payload);

		case 'LOAD_DAY_DATA_REQUEST':
			return store.updateIn(['data'], data => {
				return data.merge({
					[action.date]: {loading: true}
				})
			});

		case 'LOAD_DAY_DATA_SUCCESS':
			log.log('LOAD_DAY_DATA_SUCCESS', action.payload.date);

			return store.updateIn(['data', action.payload.date], dayData => {
				return parseDayData(action.payload.data)
			});
		default:
			return store;
	}
}