import {combineReducers} from 'redux';

import authorization from './app/authorization';
import planner from './app/planner';
import user from './app/user';

export default combineReducers({
	authorization,
	planner,
	user
});

