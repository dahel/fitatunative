import {
    Dimensions
} from 'react-native';

import colors, {CONSTANTS} from '../../theme/colors';

let screenWidth = Dimensions.get("window").width;
let screenHeight = Dimensions.get("window").height;

const menu = {
    container: {
        padding: 6
    },
    icon: {
        size: 30,
        color: colors[CONSTANTS.MINE_SHAFT],
        marginTop: 10
    }
};

export default {
    menu
}