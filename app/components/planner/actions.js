import { AsyncStorage } from 'react-native';
import { Actions } from 'react-native-router-flux';
import moment from 'moment';

import config from '../../config/settings';
import apiClient from '../../modules/apiClient';

export function fetchPlannerDataRequest(token) {
	return {
		type: 'FETCH_PLANNER_DATA_REQUEST',
		payload: token
	}
}

export function fetchPlannerDataSuccess(data) {
	return {
		type: 'FETCH_PLANNER_DATA_SUCCESS',
		payload: data
	}
}

export function fetchPlannerData(userId, date) {
	return dispatch => {
		dispatch(fetchPlannerDataRequest());

		return apiClient
			.get(`/diet-and-activity-plan/${userId}/day/${date}`)
			.then((response) => {
				return dispatch(fetchPlannerDataSuccess(response.data))
			})
			.catch(function (error) {
				console.log('################################################## error');
				console.log(error);
				// todo handle it
				// debugger;
			});
	}
}

function onChangeDate(newDate) {
	return {
		type: 'PLANNER_CHANGE_DATE',
		payload: newDate
	};
}


export function changeDate(data) {
	return dispatch => {
		dispatch(fetchPlannerDataRequest());
		dispatch(onChangeDate(data.newDate));

		return apiClient
			.get(`/diet-and-activity-plan/${data.userId}/day/${data.newDate}`)
			.then((response) => {
				return dispatch(fetchPlannerDataSuccess(response.data))
			})
			.catch(function (error) {
				console.log('################################################## error');
				console.log(error);
				// todo handle it
				// debugger;
			});
	}
}