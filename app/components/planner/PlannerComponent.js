import React, {
	Component,
	PropTypes
} from 'react';

import {
	Text,
	View,
	Image,
	Dimensions,
	TouchableWithoutFeedback
} from 'react-native';

import EntypoIcon from 'react-native-vector-icons/Entypo';

import logger from '../../modules/logger';
import moment from 'moment';

import TopBar from '../shared/topBar/TopBarContainer';
import PlannerDayCarousel from './plannerDayCarousel/PlannerDayCarousel';
import DateCarousel from './dateCarousel/DateCarousel';

import styles from './styles';

let log = logger.get('PlannerComponent');

export default class Planner extends Component {
	constructor (props) {
		super(props);

		this.renderCarousel = this.renderCarousel.bind(this);
		this.renderMenuButton = this.renderMenuButton.bind(this);
		this.onMenuButtonSelected = this.onMenuButtonSelected.bind(this);
		this.onNextPageDisplayed = this.onNextPageDisplayed.bind(this);
		this.onPreviousPageDisplayed = this.onPreviousPageDisplayed.bind(this);
		this.onPlannerCarouselScroll = this.onPlannerCarouselScroll.bind(this);
		this.onSamePageDisplayed = this.onSamePageDisplayed.bind(this);

		this.state = {
			selectedDate: moment().format('YYYY-MM-DD'),
			rendered: false
		}
	}

	componentDidMount () {
		this.props.loadInitialData({selectedDate: moment().format('YYYY-MM-DD')})
	}

	render() {
		log.log('render()', this.props.planner);

		return (
			<View>
				<TopBar
					titleConfig={TopBar.LOGO}
					leftButtonConfig={this.renderMenuButton()}
				/>
				<DateCarousel ref="dateCarousel" selectedDate={this.state.selectedDate} dates={this.props.planner.dates} />
				{this.props.planner.loading ? (
					<View>
						<Text>loading......</Text>
					</View>
				) : this.renderCarousel()}
			</View>
		);
	}

	renderMenuButton() {

		return (
			<TouchableWithoutFeedback onPress={this.onMenuButtonSelected}>
				<View style={styles.menu.container}>
					<EntypoIcon  name="menu" size={styles.menu.icon.size} color={styles.menu.icon.color} />
				</View>
			</TouchableWithoutFeedback>
		)
	}

	onMenuButtonSelected () {

	}

	renderCarousel() {
		log.log('renderCarousel()');

		return <PlannerDayCarousel
			ref="plannerCarousel"
			onNextPageDisplayed={this.onNextPageDisplayed}
			onPreviousPageDisplayed={this.onPreviousPageDisplayed}
			onSamePageDisplayed={this.onSamePageDisplayed}
			plannerData={this.props.planner}
			onScroll={this.onPlannerCarouselScroll}
		/>;
	}

	onPageScrolled () {
		log.log('onPageScrolled()');
	}

	onNextPageDisplayed () {
		log.log('onNextPageDisplayed()');

		let newDate = moment(this.state.selectedDate).add('days', 1).format('YYYY-MM-DD');

		this.refs.dateCarousel.updateScroll(newDate);

		this.setState({
			selectedDate: newDate
		});

		var requestDate = moment(newDate).add('days', 2).format('YYYY-MM-DD');
		this.props.loadDayData(requestDate);
	}

	onPreviousPageDisplayed () {
		log.log('onPreviousPageDisplayed()');

		let newDate = moment(this.state.selectedDate).subtract('days', 1).format('YYYY-MM-DD');

		this.refs.dateCarousel.updateScroll(newDate);



		this.setState({
			selectedDate: newDate
		});

		var requestDate = moment(newDate).subtract('days', 2).format('YYYY-MM-DD');
		this.props.loadDayData(requestDate);
	}

	onPlannerCarouselScroll (scrollOffset) {
		log.log('onPlannerCarouselScroll', scrollOffset);

		this.refs.dateCarousel.addScrollOffset(scrollOffset / 4);
	}

	onSamePageDisplayed () {
		this.refs.dateCarousel.updateScroll(this.state.selectedDate);
	}
}
