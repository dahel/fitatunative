import colors, {CONSTANTS} from '../../../theme/colors';

const container = {
	width: 90,
	height: 37,
	marginTop: 10
};

const topRow = {
	textAlign: 'center',
	fontSize: 16
};

const currentDayTopRow = Object.assign({}, topRow,{
	color: colors[CONSTANTS.SILVER_CHALICE]
});

const bottomRow = {
	textAlign: 'center',
	fontSize: 12,
	marginTop: -5
};

const currentDayBottomRow = Object.assign({}, bottomRow,{
	color: colors[CONSTANTS.SILVER_CHALICE]
});

const currentDayContainer = Object.assign({}, container, {
	borderBottomWidth: 2,
	borderBottomColor: colors[CONSTANTS.MINE_SHAFT]
});

export default {
	container,
	topRow,
	bottomRow,
	currentDayContainer,
	currentDayTopRow,
	currentDayBottomRow
}