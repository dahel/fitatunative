import React, {
    Component,
    PropTypes
} from 'react';

import {
    Text,
    View,
    Image,
    Dimensions,
    ScrollView
} from 'react-native';

import moment from 'moment';
import styles from './styles';

export default class DateCarouselItem extends Component {
    constructor (props) {
        super(props);

        this.getDateDescription = this.getDateDescription.bind(this);
        this.getStyles = this.getStyles.bind(this);
    }


    render() {
        let width = Dimensions.get("window").width;
        let height = Dimensions.get("window").height;

        return (
            <View style={this.getStyles()}>
                <Text style={this.getTopRowStyles()}>{this.getDateDescription()}</Text>
                <Text style={this.getBottomRowStyles()}>{moment(this.props.date).format('dddd')}</Text>
            </View>
        )
    }

    getStyles () {
        if (this.isSelectedDate()) {
            return styles.currentDayContainer;
        } else {
            return styles.container;
        }
    }

    getTopRowStyles () {
        if (this.isSelectedDate()) {
            return styles.topRow;
        } else {
            return styles.currentDayTopRow;
        }
    }

    getBottomRowStyles () {
        if (this.isSelectedDate()) {
            return styles.bottomRow;
        } else {
            return styles.currentDayBottomRow;
        }
    }

    isSelectedDate() {
        return moment(this.props.selectedDate).diff(this.props.date, 'days') === 0;
    }

    getDateDescription () {
        let result;

        if (moment().diff(this.props.date, 'days') < 2 && moment().diff(this.props.date, 'days') > -1) {
            result = moment(this.props.date).calendar(true);
        } else {
            result = moment(this.props.date).format('DD MMM');
        }

        return result;
    }
}
