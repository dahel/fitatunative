import {
	Dimensions
} from 'react-native';

import colors, {CONSTANTS} from '../../../theme/colors';

let screenWidth = Dimensions.get("window").width;

const headerContainer = {
	flex: 1,
	flexDirection: 'row',
	height: 40,
	marginTop: 30,
	paddingLeft: 15,
	paddingRight: 15
};

const leftContainer = {
	flex: 3,
	height: 40
};

const name = {
	container: {
		flex: 1,
		flexDirection: 'row',
		height: 18
	},
	text: {
		marginLeft: 5,
		fontSize: 16,
		color: colors[CONSTANTS.MINE_SHAFT],
		fontWeight: '100'
	}
};

const toggleIcon = {
	size: 18,
	color: colors[CONSTANTS.MINE_SHAFT],
	colorMuted: colors[CONSTANTS.ALTO]
};

const rightContainer = {
	flex: 1,
	flexDirection: 'row',
	alignItems: 'flex-end',
	height: 30
};


const summary = {
	container: {
		flex: 1,
		flexDirection: 'row',
		height: 18,
		marginLeft: 22
	}
};

const rightIconsContainer = {
	flex: 1,
	flexDirection: 'row',
	justifyContent: 'center',
	alignItems: 'center',
	marginTop: 5
};

const rightIconContainer = {
	width: 38,
	height: 38,
	justifyContent: 'center',
	alignItems: 'center',
	marginLeft: 5
};

const rightIcon = {
	size: 22,
	color: colors[CONSTANTS.MINE_SHAFT]
};

const menuIcon = {
	container:  Object.assign({}, rightIconContainer, {
		backgroundColor: colors[CONSTANTS.IRON]
	})
};

const plusIcon = {
	container:  Object.assign({}, rightIconContainer, {
		backgroundColor: colors[CONSTANTS.PASTEL_GREEN]
	}),
	size: 35,
	color: colors[CONSTANTS.WHITE]
};

export default {
	headerContainer,
	leftContainer,
	rightContainer,
	toggleIcon,
	name,
	summary,
	rightIconsContainer,
	rightIconContainer,
	rightIcon,
	menuIcon,
	plusIcon
}