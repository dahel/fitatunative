import React, {
	Component,
	PropTypes
} from 'react';

import {
	Text,
	View,
	Image,
	Dimensions,
	TouchableWithoutFeedback

} from 'react-native';

import * as Animatable from 'react-native-animatable';

import Icon from 'react-native-vector-icons/FontAwesome';
import EntypoIcon from 'react-native-vector-icons/Entypo';

import styles from './styles'

export default class SectionHeader extends Component {
	constructor (props) {
		super(props);

		this.getSectionName = this.getSectionName.bind(this);

		this.state = {
			translations: {
				breakfast: 'Śniadanie',
				second_breakfast: '|| Śniadanie',
				lunch: 'Lunch',
				supper: 'Obiad',
				snack: 'Przekąska',
				dinner: 'Kolacja'
			}
		}
	}

	componentDidMount () {
		//this.fetchPlannerData();
	}

	render() {
		return (
			<View style={styles.headerContainer}>
				<TouchableWithoutFeedback style={{height: 50}} onPress={this.props.toggleCallback}>
					<View style={styles.leftContainer}>
						<View style={styles.name.container}>
							<Icon name="chevron-down" size={styles.toggleIcon.size} color={this.getIconColor()} />
							<Text style={styles.name.text}>{this.getSectionName()}</Text>
						</View>
						<View style={styles.summary.container}>
							<Text>{Math.round(this.props.summary.protein)} g | </Text>
							<Text>{Math.round(this.props.summary.fat)} g | </Text>
							<Text>{Math.round(this.props.summary.carbohydrate)} g | </Text>
							<Text>{Math.round(this.props.summary.energy)} kcal</Text>
						</View>
					</View>
				</TouchableWithoutFeedback>

				<View style={styles.rightContainer}>
					<View style={styles.rightIconsContainer}>
						<View style={styles.menuIcon.container}>
							<EntypoIcon name="dots-three-vertical" size={styles.rightIcon.size} color={styles.rightIcon.color} />
						</View>
						<View style={styles.plusIcon.container}>
							<EntypoIcon name="plus" size={styles.plusIcon.size} color={styles.plusIcon.color} />
						</View>
					</View>
				</View>
			</View>
		)
	}

	getIconColor () {
		if (this.props.itemsLength) {
		    return styles.toggleIcon.color;
		} else {
			return styles.toggleIcon.colorMuted;
		}
	}

	getSectionName () {
		return this.state.translations[this.props.sectionName];
	}
}
