import { connect } from 'react-redux';

import PlannerComponent from './PlannerComponent';

export default connect(
	function mapStateToProps(state) {
		return {
			user: state.user.toJS(),
			planner: state.planner.toJS()
		};
	},
	function mapDispatchToProps(dispatch) {
		return {
			changeDate: (date) => dispatch({
				type: 'PLANNER_CHANGE_DATE',
				date: date
			}),
			loadInitialData: (data) => dispatch({
				type: 'LOAD_INITIAL_PLANNER_DATA',
				payload: data
			}),
			loadDayData: (date) => dispatch({
				type: 'LOAD_DAY_DATA',
				date: date
			})
		}
	}
)(PlannerComponent);