import {
    Dimensions
} from 'react-native';

import colors, {CONSTANTS} from '../../../theme/colors';

let screenWidth = Dimensions.get("window").width;
let screenHeight = Dimensions.get("window").height;

const root = {
    backgroundColor: colors[CONSTANTS.IRON],
    marginTop: 7
};

const itemRoot = {
    paddingLeft: 37,
    paddingRight: 22,
    paddingTop: 15,
    paddingBottom: 15
};

const nameRow = {

};

const measureRow = {

};

const summaryRow = {
    flex: 1,
    flexDirection: 'row',
    height: 19
};

const name = {
    text: {

    }
};

const removeIcon = {
    container: {
        flex: 1
    },
    icon: {
        size: 20,
        color: colors[CONSTANTS.SILVER_CHALICE]
    },
    styles: {
        position: 'absolute',
        right: 0,
        top: 0
    }
};

export default {
    root,
    itemRoot,
    nameRow,
    removeIcon,
    name,
    measureRow,
    summaryRow
}