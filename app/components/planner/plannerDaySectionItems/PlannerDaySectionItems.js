import React, {
    Component,
    PropTypes
} from 'react';

import {
    Text,
    View,
    Image,
    Dimensions,
    TouchableWithoutFeedback

} from 'react-native';

import * as Animatable from 'react-native-animatable';

import Icon from 'react-native-vector-icons/FontAwesome';
import EntypoIcon from 'react-native-vector-icons/Entypo';

import styles from './styles'

//const Item = (props) => {
//    console.log('Item()', props);
//
//    return <View>
//        <View style={styles.nameRow}>
//            <View style={styles.name.container}>
//                <Text style={styles.name.text}>asdfasdfa sdf asdf asdf asdf asdf asdf asd fasdf </Text>
//            </View>
//            <View style={styles.removeIcon.container}>
//                <Icon name="chevron-down" size={styles.removeIcon.icon.size} color={styles.removeIcon.icon.color} />
//            </View>
//        </View>
//    </View>
//};

const Item = (props) => {

    return <View style={styles.itemRoot}>
        <View style={styles.nameRow}>
            <Text style={styles.name.text}>{props.item.name}</Text>
            <EntypoIcon name="cross" style={styles.removeIcon.styles} size={styles.removeIcon.icon.size} color={styles.removeIcon.icon.color} />
        </View>
        <View style={styles.measureRow}>
            <Text>{props.item.measureQuantity} {props.item.measureName}</Text>
        </View>
        <View style={styles.summaryRow}>
            <Text>{Math.round(props.item.protein)} g | </Text>
            <Text>{Math.round(props.item.fat)} g | </Text>
            <Text>{Math.round(props.item.carbohydrate)} g | </Text>
            <Text>{Math.round(props.item.energy)} kcal</Text>
        </View>
    </View>
};

export default class PlannerDaySectionItems extends Component {
    render() {
        let items = this.props.items.map((item) => (<Item item={item} key={item.productId || item.recipeId}/>));

        return (
            <View style={styles.root}>
                {items}
            </View>
        )
    }
}
