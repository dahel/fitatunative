import React, {
	Component,
	PropTypes
} from 'react';

import {
	Text,
	View,
	Image,
	Dimensions
} from 'react-native';

import SectionHeader from '../plannerSectionHeader/PlannerSectionHeader';
import PlannerDaySectionItems from '../plannerDaySectionItems/PlannerDaySectionItems';

import styles from './styles';

export default class PlannerDaySection extends Component {
	constructor (props) {
		super(props);

		this.toggleSection = this.toggleSection.bind(this);
		this.renderPlannerDaySectionItems = this.renderPlannerDaySectionItems.bind(this);

		this.state = {
			expanded: true
		}
	}

	componentDidMount () {
		//this.fetchPlannerData();
	}

	render() {
		return (
			<View style={styles.plannerDaySection}>
				<SectionHeader
					itemsLength={this.props.data.items.length}
					toggleCallback={this.toggleSection}
					summary={this.props.data.summary}
					sectionName={this.props.sectionName}
					expanded={this.state.expanded}
				/>
				{this.state.expanded ? this.renderPlannerDaySectionItems() : null}
			</View>
		)
	}

	renderPlannerDaySectionItems () {
		return (
			<PlannerDaySectionItems
				items={this.props.data.items}
			/>
		)
	}

	toggleSection () {
		if (this.props.data.items.length) {
			this.setState({
				expanded: !this.state.expanded
			});
		}
	}
}
