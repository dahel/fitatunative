import React, {
	Component,
	PropTypes
} from 'react';

import {
	Text,
	View,
	Image,
	Dimensions,
	ScrollView
} from 'react-native';

import moment from 'moment';

import logger from '../../../modules/logger';

import Carousel from 'react-native-looped-carousel';

import PlannerDay from '../plannerDay/PlannerDay';

const log = logger.get('PlannerDayCarousel');

// todo
// todo
// todo
// todo
// todo
// todo
// todo
// todo
// todo
// todo
// todo
// todo
// todo
// todo
// todo
// todo
// problem jest taki -> przewijasz do poczatku scrolla - scroll jest na 0.
// laduje sie kolejna strona z lewej i trzeba o te strone przeskrolowac widok w prawo
// a teraz nie jest przeskrolowany i jako aktualnie wyświetlana strona jest ta dopiero zaladowana

export default class Planner extends Component {
	constructor (props) {
		super(props);

		this.generatePlannerDays = this.generatePlannerDays.bind(this);
		//this.setInitialScroll = this.setInitialScroll.bind(this);
		// this.onScrollRight = this.onScrollRight.bind(this);
		// this.onScrollLeft = this.onScrollLeft.bind(this);
		this.onScroll = this.onScroll.bind(this);
		this.updateScroll = this.updateScroll.bind(this);
		this.calculateCurrentScroll = this.calculateCurrentScroll.bind(this);

		this.state = {
			windowWidth: Dimensions.get("window").width,
			currentPageIndex: 2
		}
	}

	componentDidMount () {
		setTimeout(this.updateScroll.bind(this),  500);
	}


	render() {
		let width = Dimensions.get("window").width;
		let height = Dimensions.get("window").height;

		let items = this.generatePlannerDays();

		log.log('render()', items);

		return (
			<ScrollView
				ref='scrollView'
				horizontal={true}
				pagingEnabled={true}
				onScroll={this.onScroll}
			>
				{items}
			</ScrollView>
		)
	}

	updateScroll () {
		this.refs.scrollView.getScrollResponder().scrollTo({x: this.calculateCurrentScroll(), y: 250, animated: false});
	}

	generatePlannerDays () {
		let keys = Object.keys(this.props.plannerData.data).sort();

		log.log('generatePlannerDays()');

		return keys.map((key) => {
			return <PlannerDay dayData={this.props.plannerData.data[key]} date={key} key={key} />;
		})
	}

	calculateCurrentScroll () {
		let index = Object.keys(this.props.plannerData.data).sort().indexOf(this.props.plannerData.selectedDate);
		let width = Dimensions.get("window").width;

		return width * index;
	}


	onScroll (event) {
		let scrollValue = event.nativeEvent.contentOffset.x;
		let scrollOffset = scrollValue - this.state.windowWidth * this.state.currentPageIndex;

		if (scrollValue % this.state.windowWidth === 0) {
			this.onPageScrolled(scrollValue);
		} else {
			this.props.onScroll(scrollOffset);
		}
	}

	onPageScrolled (scrollValue) {
		let currentPageIndex = scrollValue / this.state.windowWidth;

		log.log('onPageScrolled() currentPageIndex:', currentPageIndex);

		if (currentPageIndex < this.state.currentPageIndex) {
			this.props.onPreviousPageDisplayed();
		} else if (currentPageIndex > this.state.currentPageIndex) {
			this.props.onNextPageDisplayed();
		} else {
			this.props.onSamePageDisplayed();
			log.log('onPageScrolled() Page is the same!')
		}

		this.setState({currentPageIndex: currentPageIndex});
	}
}
