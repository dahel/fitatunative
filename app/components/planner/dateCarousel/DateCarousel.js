import React, {
    Component,
    PropTypes
} from 'react';

import {
    Text,
    View,
    Image,
    Dimensions,
    ScrollView
} from 'react-native';

import moment from 'moment';
import momentRange from 'moment-range';
import logger from '../../../modules/logger';

import DateCarouselItem from '../dateCarouselItem/DateCarouselItem';

import carouselItemStyles from '../dateCarouselItem/styles';

const log = logger.get('DateCarousel');

export default class DateCarousel extends Component {
    constructor (props) {
        super(props);

         this.updateScroll = this.updateScroll.bind(this);
        //
         this.state = {
         	currentScroll: 0
         }
    }

    componentDidMount () {
        setTimeout(this.updateScroll.bind(this), 1);
    }

    // shouldComponentUpdate () {
    //     return false;
    // }

    updateScroll (date) {
        let index = this.props.dates.indexOf(date || this.props.selectedDate);
        let width = Dimensions.get("window").width;
        let scrollValue = (carouselItemStyles.container.width * index) - (width / 2) + (carouselItemStyles.container.width / 2);

        this.refs.scrollView.getScrollResponder().scrollTo({
            x: scrollValue,
            animated: true
        });

        log.log('updateScroll()', date || this.props.selectedDate, scrollValue);

        this.setState({currentScroll: scrollValue});
    }

    addScrollOffset (scrollOffset) {
        log.log('addScrollOffset()', this.state.currentScroll, scrollOffset);



        let scrollValue = this.state.currentScroll + scrollOffset;

        this.refs.scrollView.getScrollResponder().scrollTo({
            x: scrollValue,
            animated: true
        });

        // this.setState({currentScroll: scrollValue});
    }

    render() {
        let width = Dimensions.get("window").width;
        let height = Dimensions.get("window").height;

        let items = this.props.dates.map(date =>{
            return <DateCarouselItem
                date={date}
                key={date}
                selectedDate={this.props.selectedDate}
            />;
        });

        return (
            <ScrollView
                ref="scrollView"
                directionalLockEnabled={true}
                horizontal={true}
                showsHorizontalScrollIndicator={false}
                style={{ flex: 1, height: 60 }}>
                {items}
            </ScrollView>
        )
    }

    onClick () {

    }
}
