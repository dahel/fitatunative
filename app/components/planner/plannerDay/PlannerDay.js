import React, {
	Component,
	PropTypes
} from 'react';

import {
	Text,
	View,
	Image,
	Dimensions
} from 'react-native';

import moment from 'moment';

import logger from '../../../modules/logger';

import Spinner from 'react-native-loading-spinner-overlay';
import PlannerDaySection from '../plannerDaySection/PlannerDaySection';

const log = logger.get('PlannerDay');

export default class Planner extends Component {
	constructor (props) {
		super(props);

		this.generateSections = this.generateSections.bind(this);
		//
		// this.state = {
		// 	selectedDate: moment().format('YYYY-MM-DD')
		// }
	}

	componentDidMount () {
		//this.fetchPlannerData();
	}

	fetchPlannerData () {
		//this.props.fetchPlannerData(this.props.user.id, this.state.selectedDate)
	}

	render () {
		//log.log('render()', this.props.date, this.props.dayData);

		if (this.props.dayData.loading) {
		    return this.renderLoading();
		} else {
			return this.renderSections();
		}
	}

	renderSections() {
		let width = Dimensions.get("window").width;

		return (
			<View style={{width: width}}>
				{this.generateSections(this.props.dayData)}
			</View>
		)
	}

	generateSections (dayData) {
		 return Object.keys(dayData.dietPlan).map((key) => {
		 	return <PlannerDaySection data={dayData.dietPlan[key]} key={key} sectionName={key} />
		 });

		//return <PlannerDaySection data={plannerData.dietPlan['breakfast']} key={'breakfast'} sectionName={'breakfast'} />
	}

	renderLoading () {
		return (
			<View>
				<Text>Loading...</Text>
			</View>
		);
	}

}
