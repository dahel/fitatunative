import React, {
	Component,
	PropTypes
} from 'react';

import {
	Text,
	View,
	TouchableHighlight,
	Dimensions,
	AsyncStorage
} from 'react-native';

import { connect } from 'react-redux';
import { Actions } from 'react-native-router-flux';


import Spinner from 'react-native-loading-spinner-overlay';


class Initialization extends Component {
	constructor(props) {
		super(props);

		this.state = {
			loading: true
		}
	}

	componentDidMount () {

		 //AsyncStorage.removeItem('token', (error, token) => {
		 //	console.log('Logout success');
		 //});

		AsyncStorage.getItem('token', (error, token) => {
			this.setState({loading: false});

			// this.props.fetchUserData();

			if (token) {
				this.props.dispatch({
					type: 'INITIALIZATION_COMPLETE',
					payload: token
				});
				Actions.planner();
			} else {
				Actions.authorization();
			}
		});
	}

	render() {
		return (
			<View>
				
			</View>
		);
	}
}

export default connect()(Initialization);