import { connect } from 'react-redux';

import InitializationComponent from './InitializationComponent ';

import {fetchUserData} from './actions';

export default connect(
	function mapStateToProps(state) {
		return {
			authorization: state.authorization.toJS()
		};
	}
	//function mapDispatchToProps(dispatch) {
	//	return {
	//		fetchUserData: (userId) => dispatch(fetchUserData(userId))
	//	}
	//}
)(InitializationComponent);