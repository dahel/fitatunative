import React, {
    Component,
    PropTypes
} from 'react';

import {
    Text,
    View,
    Image,
    TextInput
} from 'react-native';

import styles from './styles';

export default class Input extends Component {
    constructor(props) {
        super(props);

    }

    render() {
        return (
            <View style={this.props.style ? this.props.style.container : null}>
                {this.props.topLabel ? this.renderTopLabel() : null}
                <TextInput
                    ref="input"
                    underlineColorAndroid='rgba(0,0,0,0)'
                    style={styles.input}
                    onChangeText={this.props.onChangeText}
                    value={this.props.value}
                    secureTextEntry={!!this.props.secureTextEntry}
                />
            </View>
        );
    }

    renderTopLabel () {
        return <Text style={styles.topLabel}>
            {this.props.topLabel}
        </Text>
    }
}

Input.propTypes = {
    onChangeText: PropTypes.func.isRequired,
    secureTextEntry: PropTypes.bool,
    value: PropTypes.string,
    topLabel: PropTypes.string,
    //onPress: PropTypes.func.isRequired,
    styles: PropTypes.object
};