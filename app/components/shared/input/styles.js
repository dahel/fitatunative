import colors, {lighten, CONSTANTS} from '../../../theme/colors';

const input = {
    height: 35,
    borderBottomColor: 'black',
    borderBottomWidth: 1

};

const topLabel = {
    color: colors[CONSTANTS.EDWARD]
};

export default {
    input,
    topLabel
}