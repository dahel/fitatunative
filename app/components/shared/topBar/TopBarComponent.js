import React, {
    Component,
    PropTypes
} from 'react';

import {
    Text,
    View,
    Image,
    TouchableHighlight
} from 'react-native';

import { Actions } from 'react-native-router-flux';

import {topBar} from '../../../config/images';

import NavigationBar from 'react-native-navbar';

import styles from './styles';

const logo = (<Image style={{width: 100, height: 29}} source={topBar.logo2x}/>);


export default class TopBar extends Component {
    static LOGO = logo;

    constructor(props) {
        super(props);
    }

    render() {
        return (
            <NavigationBar
                title={this.props.titleConfig}
                rightButton={this.props.rightButtonConfig}
                leftButton={this.props.leftButtonConfig}
            />
        );
    }
}

TopBar.propTypes = {
    titleConfig: PropTypes.object,
    rightButtonConfig: PropTypes.object,
    leftButtonConfig: PropTypes.object
};

TopBar.defaultProps = {
    titleConfig: {
        title: ''
    },
    rightButtonConfig: {
        title: ''
    },
    leftButtonConfig: {
        title: 'Wstecz',
        handler: () => {
            Actions.pop();
        }
    }
};