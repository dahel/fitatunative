import {
    Dimensions
} from 'react-native';

import colors, {CONSTANTS} from '../../../theme/colors';
import text from '../../../theme/text';

let screenWidth = Dimensions.get("window").width;

let buttonWidth = 100;

let container = {
    backgroundColor: colors[CONSTANTS.PORCELAIN],
    flex: 1,
    flexDirection: 'row',
    height: 50
};

const backButton = {
    container: {
        flex: 1,
        flexDirection: 'row',
        height: 50,
        width: 100,
        alignItems: 'center'
    },
    icon: {
         margin: 15,
        color: text.link
    }
};

const centerButton = {
    container: {
        width: screenWidth - buttonWidth * 2,
        alignItems: 'center',
        justifyContent: 'center'
    },
    text: {
        fontSize: 18
    }
};

const rightButton = {
    container: {
        height: 50,
        width: 100,
        alignItems: 'center',
        justifyContent: 'center'
    },
    text: {
        fontSize: 18,
        color: text.link
    }
};

export default topBar = {
    container,
    backButton,
    rightButton,
    centerButton
}