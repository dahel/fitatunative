import colors, {lighten, CONSTANTS} from '../../../theme/colors';

const button = {
    container: {
        alignItems: 'center',
        justifyContent: 'center',
        padding: 10
    },
    text: {
        color: colors[CONSTANTS.WHITE],
        fontSize: 16
    }
};

const primary = {
    container: Object.assign({}, button.container, {
        backgroundColor: colors[CONSTANTS.PASTEL_GREEN]
    }),
    text: Object.assign({}, button.text, {

    }),
    underlayColor: lighten(colors[CONSTANTS.PASTEL_GREEN])
};

const secondary = {
    container: Object.assign({}, button.container, {
        backgroundColor: colors[CONSTANTS.SAN_MARINO]
    }),
    text: Object.assign({}, button.text, {
    }),
    underlayColor: lighten(colors[CONSTANTS.SAN_MARINO])
};

const grey = {
    container: Object.assign({}, button.container, {
        backgroundColor: colors[CONSTANTS.ALTO]
    }),
    text: Object.assign({}, button.text, {
        color: colors[CONSTANTS.MINE_SHAFT]
    }),
    underlayColor: lighten(colors[CONSTANTS.ALTO])
};


export default {
    primary,
    grey,
    secondary
}