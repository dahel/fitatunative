import React, {
    Component,
    PropTypes
} from 'react';

import {
    Text,
    View,
    Image,
    TouchableHighlight
} from 'react-native';

import styles from './styles';

export default class Button extends Component {
    static PRIMARY = 'primary';
    static SECONDARY = 'secondary';
    static GREY = 'grey';

    constructor(props) {
        super(props);

        this.onPress = this.onPress.bind(this);
    }

    render() {
        return (
            <TouchableHighlight
                onPress={this.onPress}
                style={this.composeButtonStyles()}
                underlayColor={styles[this.props.type].underlayColor}
            >
                <Text style={styles[this.props.type].text}>{this.props.children}</Text>
            </TouchableHighlight>
        );
    }

    composeButtonStyles () {
        const defaultStyles = styles[this.props.type].container;

        return Object.assign({}, defaultStyles, this.props.style);
    }

    onPress () {
        this.props.onPress();
    }
}

Button.propTypes = {
    type: PropTypes.string.isRequired,
    onPress: PropTypes.func.isRequired,
    styles: PropTypes.object
};