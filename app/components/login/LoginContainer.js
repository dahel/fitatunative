import { connect } from 'react-redux';

import LoginComponent from './LoginComponent';

export default connect(
	function mapStateToProps(state) {
		return {
			authorization: state.authorization.toJS()
		};
	},
	function mapDispatchToProps(dispatch) {
		return {
			login: (credentials) => dispatch({
				type: 'LOGIN',
				credentials: credentials
			})
		}
	}
)(LoginComponent);