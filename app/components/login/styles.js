import {
    Dimensions
} from 'react-native';

let screenWidth = Dimensions.get("window").width;
let screenHeight = Dimensions.get("window").height;

export default {
    logoWrapper: {
        width: screenWidth,
        height: screenHeight / 3,
        flex: 1,
        alignItems: 'center',
        justifyContent:'center'
    },
    formContainer: {
        padding: 20
    },
    input: {
        container: {
            marginTop: 20
        }
    }
}