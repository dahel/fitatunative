import React, {
	Component,
	PropTypes
} from 'react';

import {
	Text,
	View,
	Image
} from 'react-native';

import Spinner from 'react-native-loading-spinner-overlay';

import logger from '../../modules/logger';
import TopBar from '../shared/topBar/TopBarContainer';
import Input from '../shared/input/Input';

import styles from './styles';

const log = logger.get('LoginComponent');

export default class Login extends Component {
	constructor (props) {
		super(props);

		this.onUsernameChange = this.onUsernameChange.bind(this);
		this.onPasswordChange = this.onPasswordChange.bind(this);
		this.onLoginButtonSelected = this.onLoginButtonSelected.bind(this);

		this.state = {
			_username: 'barman@wp.pl',
			_password: 'barman'
		}
	}
	render() {
		return (
			<View>
				<Spinner visible={this.props.authorization.loading} />
				<TopBar
					titleConfig={{
						title: 'Mam konto'
					}}
					rightButtonConfig={{
						title: 'Zaloguj',
						handler: this.onLoginButtonSelected.bind(this)
					}}
				/>
				<View style={styles.formContainer}>
					<Input
						ref="usernameInput"
						style={styles.input}
						topLabel="Adres e-mail"
						value={this.state._username}
						onChangeText={this.onUsernameChange}
					/>
					<Input
						ref="passwordInput"
						style={styles.input}
						topLabel="Hasło"
						secureTextEntry={true}
						value={this.state._password}
						onChangeText={this.onPasswordChange}
					/>
				</View>
			</View>
		);
	}

	onUsernameChange (text) {
		this.setState({_username: text});
	}

	onPasswordChange (text) {
		this.setState({_password: text});
	}

	onLoginButtonSelected () {
		log.log('onLoginButtonSelected()', this.state);

		this.props.login(this.state);
	}
}