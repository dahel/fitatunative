import React, { Component } from 'react';
import {
	AppRegistry,
	StyleSheet,
	Text,
	View,
	Navigator
} from 'react-native';

import { Router, Scene, ActionConst  } from 'react-native-router-flux';

import Authorization from '../components/authorization/AuthorizationContainer';
import Login from '../components/login/LoginContainer';
import Planner from '../components/planner/PlannerContainer';
import Initialization from '../components/initialization/InitializationComponent';

export default class App extends Component {
	render() {
		return (
			<Router>
				<Scene key="root">
					<Scene key="initialization" component={Initialization} initial={true} title="" hideNavBar={true}/>
					<Scene key="authorization" type={ActionConst.REPLACE}  component={Authorization} title="" hideNavBar={true}/>
					<Scene key="login" component={Login} title="" hideNavBar={true}/>
					<Scene key="planner" type={ActionConst.REPLACE} component={Planner} title="" hideNavBar={true}/>
				</Scene>
			</Router>
		)
	}
}