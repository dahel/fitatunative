import { Actions } from 'react-native-router-flux';

export function navigateToLogin() {
	Actions.login();

	return {
		type: 'NAVIGATE_TO_LOGIN'
	}
}