import { connect } from 'react-redux';

import AuthorizationComponent from './AuthorizationComponent';

import {navigateToLogin} from './actions';

export default connect(
	function mapStateToProps(state) {
		return {

		};
	},
	function mapDispatchToProps(dispatch) {
		return {
			navigateToLogin: () => dispatch(navigateToLogin())
		}
	}
)(AuthorizationComponent);