import React, {
	Component,
	PropTypes
} from 'react';

import {
	Text,
	View,
	Image
} from 'react-native';

import Button from '../shared/button/Button';

import {authorization} from '../../config/images';

import styles from './styles';


export default class Authorization extends Component {
	constructor (props) {
		super(props);

		this.onButtonLoginPress = this.onButtonLoginPress.bind(this);
	}

	render() {
		return (
			<View>
				<View style={styles.logoWrapper}>
					<Image width="171" height="71"
						source={authorization.logo2x}
					/>
				</View>
				<View style={styles.buttonsContainer}>
					<Button onPress={() => {}} style={styles.button} type={Button.GREY}>Wypróbuj bez logowania</Button>
					<Button onPress={this.onButtonLoginPress} style={styles.button} type={Button.PRIMARY}>Zaloguj</Button>
					<Button onPress={() => {}} style={styles.button} type={Button.PRIMARY}>Zarejestruj</Button>
					<Button onPress={() => {}} style={styles.button} type={Button.SECONDARY}>Zaloguj przez Facebook</Button>
				</View>
			</View>
		);
	}

	onButtonLoginPress () {
		this.props.navigateToLogin();
	}
}