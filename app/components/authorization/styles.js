import {
    Dimensions
} from 'react-native';

import colors, {CONSTANTS} from '../../theme/colors';

let screenWidth = Dimensions.get("window").width;
let screenHeight = Dimensions.get("window").height;

export default {
    logoWrapper: {
        width: screenWidth,
        height: screenHeight / 2.3,
        flex: 1,
        alignItems: 'center',
        justifyContent:'center'
    },

    buttonsContainer: {
        paddingLeft: 35,
        paddingRight: 35
    },
    button: {
        margin: 5
    }
}