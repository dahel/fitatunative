import axios from 'axios';

import config from '../config/settings';

const apiClient = axios.create({
	baseURL: config.API_URL,
	headers: {
		'Cache-Control': 'no-cache',
		'Accept': 'application/json',
		'Content-Type': 'application/json',
		'API-Key': 'FITATU-MOBILE-APP',
		'API-Secret': 'PYRXtfs88UDJMuCCrNpLV'
	}
});

export default apiClient;