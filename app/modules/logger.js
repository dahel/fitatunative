import logger from 'js-logger';

const defaultPadding = 20;
let firstLogTime = 0;

function addPadding(name) {
    var padding = defaultPadding - name.length,
        i = 0,
        value = '';

    for (i; i < padding; i++) {
        value += '-';
    }

    return value + '>';
}

function getTimeFromLastLog() {
    var time = 0;
    if (!firstLogTime) {
        firstLogTime = new Date().getTime();
    } else {
        time = ((new Date().getTime() - firstLogTime) / 1000).toFixed(2);
    }

    return time;
}

logger.useDefaults({
    formatter: function (messages, context) {
        var date = new Date();
        messages.unshift(date.toLocaleTimeString() + '::' + date.getMilliseconds() + ' >');
        if (context.name) {
            messages.unshift('%c ' + context.name + ' ', 'background: #353535; border-radius: 3px; color: #fff', addPadding(context.name), '[' + getTimeFromLastLog() + ']');
        }
    }
});

//logger.setLevel(logger.OFF);

export default logger;