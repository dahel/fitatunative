import Symbol from 'es6-symbol';
import tinycolor from 'tinycolor2';

export function lighten(color, amount=5) {
    return tinycolor(color).lighten(amount).toString();
}

export const CONSTANTS = {
    BLACK: Symbol(),
    MINE_SHAFT: Symbol(),
    DOVE_GRAY: Symbol(),
    SILVER_CHALICE: Symbol(),
    EDWARD: Symbol(),
    ALTO: Symbol(),
    IRON: Symbol(),
    PORCELAIN: Symbol(),
    WHITE: Symbol(),
    SAN_MARINO: Symbol(),
    DODGER_BLUE: Symbol(),
    MALIBU: Symbol(),
    BOUQUET: Symbol(),
    FAIR_PINK: Symbol(),
    RED: Symbol(),
    SCARLET: Symbol(),
    NEON_CARROT: Symbol(),
    PIZAZZ: Symbol(),
    CONIFER: Symbol(),
    PASTEL_GREEN: Symbol(),
    FOREST_GREEN: Symbol(),
    LIGHT_GREY: Symbol()
};

export default {
    [CONSTANTS.BLACK]: '#000',
    [CONSTANTS.MINE_SHAFT]: '#333',
    [CONSTANTS.DOVE_GRAY]: '#666',
    [CONSTANTS.SILVER_CHALICE]: '#9f9f9f',
    [CONSTANTS.EDWARD]: '#A3B1B2',
    [CONSTANTS.ALTO]: '#ddd',
    [CONSTANTS.IRON]: '#F3F4F5',
    [CONSTANTS.PORCELAIN]: '#F7F8F9',
    [CONSTANTS.WHITE]: '#fff',
    [CONSTANTS.SAN_MARINO]: '#4F73AE',
    [CONSTANTS.DODGER_BLUE]: '#2db0f7',
    [CONSTANTS.MALIBU]: '#69D3FB',
    [CONSTANTS.BOUQUET]: '#A86FC0',
    [CONSTANTS.FAIR_PINK]: '#FFE8E8',
    [CONSTANTS.RED]: '#FF3939',
    [CONSTANTS.SCARLET]: '#FF1515',
    [CONSTANTS.NEON_CARROT]: '#FFA04A',
    [CONSTANTS.PIZAZZ]: '#f80',
    [CONSTANTS.CONIFER]: '#AADB3F',
    [CONSTANTS.PASTEL_GREEN]: '#69D34A',
    [CONSTANTS.FOREST_GREEN]: '#24BD2F',
    [CONSTANTS.LIGHT_GREY]: '#C7D0D0'
}
