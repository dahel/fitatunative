import { createStore, applyMiddleware, compose } from 'redux';
import thunkMiddleware from 'redux-thunk';
import createSagaMiddleware from 'redux-saga';

import rootSaga from '../sagas';
import reducer from '../reducers';

const sagaMiddleware = createSagaMiddleware();

export default function configureStore(initialState) {
	let store = createStore(
		reducer,
		initialState,
		compose(
			applyMiddleware(thunkMiddleware, sagaMiddleware)
		)
	);

	sagaMiddleware.run(rootSaga);

	return store;
}