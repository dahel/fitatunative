import { take, put } from 'redux-saga/effects'
import { Actions } from 'react-native-router-flux';
import {
    AsyncStorage
} from 'react-native';

import apiClient from '../../modules/apiClient';

function saveToken(token) {
    return new Promise((resolve) => {
        AsyncStorage.setItem('token', token, () => resolve(token));
    })
}

export function* login(credentials) {
    try {
        yield put({type: 'LOGIN_REQUEST'});
        const response = yield apiClient.post('/login', credentials);
        const token = yield saveToken(response.data.token);
        yield put({type: 'LOGIN_SUCCESS', payload: token});

        Actions.planner();
    } catch (error) {
        let {status} = error.response;

        if (status === 422) {
            alert('Invalid credentials');
        } else {
            alert('Cannot login, unknown error');
        }

        yield put({type: 'LOGIN_FAILURE'});
    }
}

export function* watchForLogin() {
    while (true) {
        const action = yield take('LOGIN');
        yield login(action.credentials);
    }
}

