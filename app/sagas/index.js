import {watchForLogin} from './authorization/login';
import {
    watchForLoadInitialDataRequest,
    watchForLoadDayRequest
} from './planner/plannerData';

export default function* rootSaga() {
    yield [
        watchForLogin(),
        watchForLoadInitialDataRequest(),
        watchForLoadDayRequest()
    ]
}