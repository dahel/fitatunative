import { take, put, select } from 'redux-saga/effects'
import { Actions } from 'react-native-router-flux';
import {
    AsyncStorage
} from 'react-native';
import moment from 'moment';

import logger from '../../modules/logger';

import apiClient from '../../modules/apiClient';

const log = logger.get('plannerData SAGA');

export function* fetchPlannerInitialData(data) {
    let fromDate = moment(data.selectedDate).subtract('days', 2).format('YYYY-MM-DD');
    let toDate = moment(data.selectedDate).add('days', 2).format('YYYY-MM-DD');

    try {
        const userId = yield select(state => state.user.get('id'));
        yield put({type: 'LOAD_INITIAL_PLANNER_DATA_REQUEST'});
        const response = yield apiClient.get(`/diet-and-activity-plan/${userId}/from/${fromDate}/to/${toDate}`);
        yield put({type: 'LOAD_INITIAL_PLANNER_DATA_SUCCESS', payload: response.data});

    } catch (error) {
        console.log('################################################### plannerData saga error');
        console.log(error);

        yield put({type: 'LOAD_INITIAL_PLANNER_DATA_FAILURE'});
    }
}

function* loadDayData(date) {
    log.log('loadDayData()', date);

    try {
        const plannerData = yield select(state => state.planner.getIn(['data', date]));
        
        if (plannerData) {
            return;
        }

        yield put({type: 'LOAD_DAY_DATA_REQUEST', date: date});
        const userId = yield select(state => state.user.get('id'));
        const response = yield apiClient.get(`/diet-and-activity-plan/${userId}/day/${date}`);
        yield put({type: 'LOAD_DAY_DATA_SUCCESS', payload: {date: date, data: response.data}});

    } catch (error) {
        log.error('loadDayData()', error);
    }
}

export function* watchForLoadInitialDataRequest() {
    while (true) {
        const action = yield take('LOAD_INITIAL_PLANNER_DATA');
        yield fetchPlannerInitialData(action.payload);
    }
}

export function* watchForLoadDayRequest() {
    while (true) {
        const action = yield take('LOAD_DAY_DATA');
        yield loadDayData(action.date);
    }
}



