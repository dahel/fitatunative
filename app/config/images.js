export const common = {
	logo: require('../images/app_logo.png'),
	logo2x: require('../images/app_logo@2x.png'),
	topBarLogo: require('../images/bar_logo@2x.png'),
};

export const authorization = {
	logo: common.logo,
	logo2x: common.logo
};

export const topBar = {
	logo2x: common.topBarLogo
};